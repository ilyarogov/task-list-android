package com.ilyarogov.tasklist;

public class Item {
	
	public String Title;
	public String Content;
	
	public Item(String title, String content)
	{
		Title = title;
		Content = content;
	}
	
    @Override
    public String toString() {
        return Title;
    }

}
